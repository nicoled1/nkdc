package bo.com.sintesis.basesfl.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
@Entity
@Table(name = "jhi_cuenta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cuenta implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToMany(mappedBy =  "cuenta" )
    private List<Cliente> cliente;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Integer id_in;

    @Column(name = "nombre_cuenta_vc")
    private String nombre_cuenta_vc;

    @Column(name = "cliente_documento_vc")
    private Integer cliente_documento_vc;

    @Column(name = "tipo_cuenta_vc")
    private String tipo_cuenta_vc;

    @Column(name = "tipo_moneda_vc")
    private String tipo_moneda_vc;

    @Column(name = "sucursal_vc")
    private String sucursal_vc;

    @Column(name = "fecha_registro_dt")
    private String fecha_registro_dt;

    @Column(name = "usuario_registro_vc")
    private String usuario_registro_vc;

}
