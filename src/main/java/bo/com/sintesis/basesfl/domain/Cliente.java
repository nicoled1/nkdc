package bo.com.sintesis.basesfl.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "jhi_cliente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Integer id_in;

    @Column(name = "documento_vc")
    private Integer documento_vc;


    @Column(name = "nombres_vc")
    private String nombres_vc;

    @Column(name = "paterno_vc")
    private String paterno_vc;

    @Column(name = "materno_vc")
    private String materno_vc;

    @Column(name = "fecha_nacimiento_vc")
    private Timestamp fecha_nacimiento_vc;

    @Column(name = "estado_civil_vc")
    private String estado_civil_vc;

    @Column(name = "domicilio_vc")
    private String domicilio_vc;

    @Column(name = "nacionalidad_vc")
    private String nacionalidad_vc;

    @Column(name = "fecha_registro_dt")
    private String fecha_registro_dt;

    @Column(name = "usuario_registro_vc")
    private String usuario_registro_vc;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
        name = "jhi_cliente_cuenta",
        joinColumns = {@JoinColumn(name = "cliente_id", referencedColumnName = "id_in")},
        inverseJoinColumns = {@JoinColumn(name = "cuenta_id", referencedColumnName = "id_in")}
    )
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    //private Set<File> Files = new HashSet<>();
    private List<Cuenta> cuenta;

}
