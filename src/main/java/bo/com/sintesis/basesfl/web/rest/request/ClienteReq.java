package bo.com.sintesis.basesfl.web.rest.request;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;

@Data
public class ClienteReq {

    private Integer id_in;
    //private Optional <String> uuid;
    //@Pattern(regexp = Constants.EMAIL, message = "Los 3 primeros caacteres no deben ser iguales.")
    @Nullable
    private Integer documento_vc;

    //@Pattern(regexp = Constants.PHONE, message = "El número de teléfono no es válido.")

    @Nullable
    private String nombres_vc;
    //private Optional <Integer> module;
    //@Pattern(regexp = Constants.SUBJECT, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String paterno_vc;

    //@Pattern(regexp = Constants.MESSAGE, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String materno_vc;

    //@Pattern(regexp = Constants.NAME, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private Timestamp fecha_nacimiento_vc;

    //@Pattern(regexp = Constants.EMAIL, message = "Los 3 primeros caacteres no deben ser iguales.")
    @Nullable
    private String estado_civil_vc;

    //@Pattern(regexp = Constants.PHONE, message = "El número de teléfono no es válido.")
    @Nullable
    private String domicilio_vc;

    //@Pattern(regexp = Constants.SUBJECT, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String nacionalidad_vc;

    //@Pattern(regexp = Constants.MESSAGE, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String fecha_registro_dt;

    //@Pattern(regexp = Constants.PHONE, message = "El número de teléfono no es válido.")
    @Nullable
    private String usuario_registro_vc;

}
