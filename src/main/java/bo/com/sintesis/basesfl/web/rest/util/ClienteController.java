package bo.com.sintesis.basesfl.web.rest.util;

import bo.com.sintesis.basesfl.domain.Cliente;
import bo.com.sintesis.basesfl.repository.ClienteRepository;
import bo.com.sintesis.basesfl.service.ClientService;
import bo.com.sintesis.basesfl.web.rest.request.ClienteReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping("/api")
public class ClienteController {

    private final ClientService clienteService;

    private final ClienteRepository clienteRepository;

    @GetMapping("/getLogs")
    public List<Cliente> getAllLogs() {
        return clienteService.getAllLogs();
    }

    /*@PostMapping("/createClient")
    public Map<String, Object> crearCliente(@RequestBody ClienteReq request) {
        Map<String, Object> response = new HashMap<>();
        return clienteService.createClient(request);
    }*/

    @PostMapping("/createClient")
    public ResponseEntity<Map<String, Object>> crearCliente(@RequestBody ClienteReq request) {
        Map<String, Object> response = new HashMap<>();

        try {
            // Intentar crear el cliente
            Cliente clienteCreado = clienteService.createClient(request);

            // Agregar datos de éxito al mapa de respuesta
            response.put("mensaje", "Cliente creado exitosamente");
            response.put("cliente", clienteCreado);

            // Devolver una respuesta exitosa con el mapa de respuesta
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            // Manejar errores y agregar mensajes de error al mapa de respuesta
            response.put("mensaje", "Error al crear el cliente");
            response.put("error", e.getMessage());

            // Devolver una respuesta de error con el mapa de respuesta
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
