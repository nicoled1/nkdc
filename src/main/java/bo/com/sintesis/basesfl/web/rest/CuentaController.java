package bo.com.sintesis.basesfl.web.rest;

import bo.com.sintesis.basesfl.domain.Cuenta;
import bo.com.sintesis.basesfl.repository.CuentaRepository;
import bo.com.sintesis.basesfl.service.CuentaService;
import bo.com.sintesis.basesfl.web.rest.request.CuentaReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping("/api")
public class CuentaController {

    private final CuentaService cuentaService;

    private final CuentaRepository cuentaRepository;

    @PostMapping("/createCuenta")
    public ResponseEntity<Map<String, Object>> crearCuenta(@RequestBody CuentaReq request) {
        Map<String, Object> response = new HashMap<>();
        try {
            Cuenta cuentaCreado = cuentaService.createCuenta(request);
            response.put("mensaje", "Cuenta creada exitosamente");
            response.put("cuenta", cuentaCreado);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            response.put("mensaje", "Error al crear la cuenta");
            response.put("error", e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
