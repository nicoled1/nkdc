package bo.com.sintesis.basesfl.web.rest.request;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;

@Data
public class CuentaReq {

    private Integer id_in;
    //private Optional <String> uuid;
    //@Pattern(regexp = Constants.EMAIL, message = "Los 3 primeros caacteres no deben ser iguales.")
    @Nullable
    private String nombre_cuenta_vc;

    //@Pattern(regexp = Constants.PHONE, message = "El número de teléfono no es válido.")
    private Integer cliente_documento_vc;

    @Nullable
    private String tipo_cuenta_vc;
    //private Optional <Integer> module;
    //@Pattern(regexp = Constants.SUBJECT, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String tipo_moneda_vc;

    //@Pattern(regexp = Constants.MESSAGE, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String sucursal_vc;

    //@Pattern(regexp = Constants.MESSAGE, message = "El mensaje contiene caracteres no permitidos.")
    @Nullable
    private String fecha_registro_dt;

    //@Pattern(regexp = Constants.PHONE, message = "El número de teléfono no es válido.")
    @Nullable
    private String usuario_registro_vc;

}
