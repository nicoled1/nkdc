package bo.com.sintesis.basesfl.web.rest;

import bo.com.sintesis.basesfl.config.Constants;
import bo.com.sintesis.basesfl.domain.User;
import bo.com.sintesis.basesfl.repository.UserRepository;
import bo.com.sintesis.basesfl.security.AuthoritiesConstants;
import bo.com.sintesis.basesfl.service.exceptions.EmailAlreadyUsedException;
import bo.com.sintesis.basesfl.service.UserService;
import bo.com.sintesis.basesfl.web.rest.errors.spec.LoginAlreadyUsedException;
import bo.com.sintesis.basesfl.web.rest.request.UserCreateReq;
import bo.com.sintesis.basesfl.web.rest.request.UserUpdateReq;
import bo.com.sintesis.basesfl.web.rest.response.UserRes;
import bo.com.sintesis.basesfl.web.rest.util.HeaderUtil;
import bo.com.sintesis.basesfl.web.rest.util.PaginationUtil;
import bo.com.sintesis.basesfl.web.rest.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/admin")
public class UserResource {

    private final UserService userService;

    private final UserRepository userRepository;

    @PostMapping("/users")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<UserRes> createUser(@Valid @RequestBody UserCreateReq userCreateReq) throws URISyntaxException {
        log.debug("REST request to save User: {}", userCreateReq);
        if (userRepository.findOneByLogin(userCreateReq.getLogin().toLowerCase()).isPresent()) {
            throw new LoginAlreadyUsedException();
        } else if (userRepository.findOneByEmailIgnoreCase(userCreateReq.getEmail()).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            User entity = userService.create(userCreateReq);
            return ResponseEntity
                .created(new URI("/api/admin/users/" + entity.getLogin()))
                .body(new UserRes(entity));
        }
    }

    @PutMapping("/users")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<UserRes> updateUser(@Valid @RequestBody UserUpdateReq userUpdateReq) {
        log.debug("REST request to update User: {}", userUpdateReq);

        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userUpdateReq.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userUpdateReq.getId()))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<UserRes> updatedUser = userService.updateUser(userUpdateReq);
        return ResponseUtil.wrapOrNotFound(
            updatedUser,
            HeaderUtil.createAlert("BaseProject", "A user is updated with identifier " + userUpdateReq.getId().toString(), userUpdateReq.getId().toString())
        );
    }

    @DeleteMapping("/users/{login}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteUser(@PathVariable @Pattern(regexp = Constants.LOGIN_REGEX) String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createAlert("BaesProject", "A user is deleted with identifier " + login, login))
            .build();
    }

    @GetMapping("/users/{login}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<UserRes> getUser(@PathVariable @Pattern(regexp = Constants.LOGIN_REGEX) String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseUtil.wrapOrNotFound(userRepository.findOneWithAuthoritiesByLogin(login).map(UserRes::new));
    }

    @GetMapping("/users")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<UserRes>> getAllUsers(Pageable pageable) {
        log.debug("REST request to get all User for an admin");
        final Page<UserRes> page = userRepository.findAll(pageable).map(UserRes::new);
        HttpHeaders headers = PaginationUtil.addTotalCountHttpHeaders(page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
