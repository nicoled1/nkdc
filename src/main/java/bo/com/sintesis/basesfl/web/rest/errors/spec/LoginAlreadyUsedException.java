package bo.com.sintesis.basesfl.web.rest.errors.spec;

import bo.com.sintesis.basesfl.web.rest.errors.BadRequestAlertException;
import bo.com.sintesis.basesfl.web.rest.errors.ErrorConstants;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public LoginAlreadyUsedException() {
        super(ErrorConstants.LOGIN_ALREADY_USED_TYPE, "Login name already used!", "userManagement", "userexists");
    }
}
