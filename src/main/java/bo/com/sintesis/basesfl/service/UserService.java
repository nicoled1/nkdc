package bo.com.sintesis.basesfl.service;

import bo.com.sintesis.basesfl.domain.Authority;
import bo.com.sintesis.basesfl.domain.User;
import bo.com.sintesis.basesfl.repository.AuthorityRepository;
import bo.com.sintesis.basesfl.repository.UserRepository;
import bo.com.sintesis.basesfl.security.SecurityUtils;
import bo.com.sintesis.basesfl.web.rest.request.UserCreateReq;
import bo.com.sintesis.basesfl.web.rest.request.UserUpdateReq;
import bo.com.sintesis.basesfl.web.rest.response.UserRes;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    @Transactional
    public User create(UserCreateReq request) {
        User entity = new User();
        entity.setLogin(request.getLogin());
        entity.setPassword(passwordEncoder.encode(request.getPassword()));
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
        entity.setActivated(true);
        if (request.getEmail() != null) entity.setEmail(request.getEmail().toLowerCase());
        if (request.getAuthorities() != null)
            entity.setAuthorities(request.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet()));
        userRepository.save(entity);
        this.clearUserCaches(entity);
        log.debug("Created Information for User: {}", entity);
        return entity;
    }

    @Transactional
    public Optional<UserRes> updateUser(UserUpdateReq request) {
        return Optional
            .of(userRepository.findById(request.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                    this.clearUserCaches(user);
                    user.setFirstName(request.getFirstName());
                    user.setLastName(request.getLastName());
                    if (request.getEmail() != null) {
                        user.setEmail(request.getEmail().toLowerCase());
                    }
                    user.setActivated(request.getActivated());
                    Set<Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    request
                        .getAuthorities()
                        .stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                    this.clearUserCaches(user);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                }
            )
            .map(UserRes::new);
    }

    public void deleteUser(String login) {
        userRepository
            .findOneByLogin(login)
            .ifPresent(
                user -> {
                    userRepository.delete(user);
                    this.clearUserCaches(user);
                    log.debug("Deleted User: {}", user);
                }
            );
    }

    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }
}
