package bo.com.sintesis.basesfl.service;

import bo.com.sintesis.basesfl.domain.Cuenta;
import bo.com.sintesis.basesfl.repository.CuentaRepository;
import bo.com.sintesis.basesfl.web.rest.request.CuentaReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CuentaService {

    private final CuentaRepository cuentaRepository;

    public List<Cuenta> getAllLogs() {
        return cuentaRepository.findAll();
    }

    @Transactional
    public Cuenta createCuenta(CuentaReq request) {
        System.out.print("entro al servicio");
        System.out.print("request:\n");
        System.out.print(request);
        String datetime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
        Cuenta entityCuenta = new Cuenta();

        entityCuenta.setCliente_documento_vc(request.getCliente_documento_vc());
        entityCuenta.setNombre_cuenta_vc(request.getNombre_cuenta_vc());
        entityCuenta.setTipo_cuenta_vc(request.getTipo_cuenta_vc());
        entityCuenta.setTipo_moneda_vc(request.getTipo_moneda_vc());
        entityCuenta.setSucursal_vc(request.getSucursal_vc());
        entityCuenta.setFecha_registro_dt(datetime);
        entityCuenta.setUsuario_registro_vc("sqlserver");
        cuentaRepository.save(entityCuenta);
        log.debug("Created Information for Client: {}", entityCuenta);
        return entityCuenta;
    }

}
