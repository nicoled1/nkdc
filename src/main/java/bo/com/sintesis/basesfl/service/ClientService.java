package bo.com.sintesis.basesfl.service;

import bo.com.sintesis.basesfl.domain.Cliente;
import bo.com.sintesis.basesfl.repository.ClienteRepository;
import bo.com.sintesis.basesfl.web.rest.request.ClienteReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ClientService {

    private final ClienteRepository clienteRepository;

    public List<Cliente> getAllLogs() {
        return clienteRepository.findAll();
    }

    @Transactional
    public Cliente createClient(ClienteReq request) {
        System.out.print("entro al servicio");
        System.out.print("request:\n");
        System.out.print(request);
        String datetime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
        Cliente entityClient = new Cliente();


        entityClient.setDocumento_vc(request.getDocumento_vc());
        entityClient.setNombres_vc(request.getNombres_vc());
        entityClient.setPaterno_vc(request.getPaterno_vc());
        entityClient.setMaterno_vc(request.getMaterno_vc());
        entityClient.setFecha_nacimiento_vc(request.getFecha_nacimiento_vc());
        entityClient.setEstado_civil_vc(request.getEstado_civil_vc());
        entityClient.setDocumento_vc(request.getDocumento_vc());
        entityClient.setNacionalidad_vc(request.getNacionalidad_vc());
        entityClient.setFecha_registro_dt(datetime);
        entityClient.setUsuario_registro_vc("sqlserver");
        clienteRepository.save(entityClient);
        log.debug("Created Information for Client: {}", entityClient);
        return entityClient;

    }

}
