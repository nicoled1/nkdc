
package bo.com.sintesis.basesfl.repository;

import bo.com.sintesis.basesfl.domain.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CuentaRepository extends JpaRepository<Cuenta, Integer> {}



