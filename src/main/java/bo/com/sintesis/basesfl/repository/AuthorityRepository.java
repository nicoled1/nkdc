package bo.com.sintesis.basesfl.repository;

import bo.com.sintesis.basesfl.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, String> {}
