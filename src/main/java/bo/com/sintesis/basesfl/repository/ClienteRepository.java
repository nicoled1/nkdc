
package bo.com.sintesis.basesfl.repository;

import bo.com.sintesis.basesfl.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClienteRepository extends JpaRepository<Cliente, Integer> {}



