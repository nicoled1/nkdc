package bo.com.sintesis.basesfl;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("bo.com.sintesis.basesfl");

        noClasses()
            .that()
            .resideInAnyPackage("bo.com.sintesis.basesfl.service..")
            .or()
            .resideInAnyPackage("bo.com.sintesis.basesfl.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..bo.com.sintesis.basesfl.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
